# Wrs::Events

Wrs::Events integrates SNS and SQS as a Pub/Sub client where publishing and subscribing is decoupled between both services. This achieves flexibility of fanout publishing with reliability of having an MQ as a subscriber. 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'wrs-events'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install wrs-events

Add following configurations to `config/initializers/wrs-events.rb`:

```ruby
# Gem Configurtions
Wrs::Events.configure do |config|

  # AWS credentials for a user with SNSFullAccess and SQSFullAccess policies attached
  config.aws_access_key_id = <YOUR_AWS_ACCESS_KEY_ID>             # required
  config.aws_secret_access_key = <YOUR_AWS_SECRET_ACCESS_KEY>     # required
  config.aws_region = <YOUR_AWS_REGION>                           # required

  # Your application name (to uniquely identify your application's topics)
  config.app_name = 'app_name'                                    # required

  # Configure suffix for the queue name of your application. Defaults to the value of `Rails.env`
  # Default queue name that your application will listen to for events is `<config.app_name>_<config.queue_suffix>`
  # config.queue_suffix = Rails.env                               # optional

  # Name of the Event Handler that is triggered when a message is received (defaults to 'EventHandler').
  # You can optionally also give a custom method name to be used for the class (defaults to 'process_messages').
  # WIP: Only class name is supported currently
  # config.event_handler = 'EventHandler' # >process_messages'    # optional

  # Use this configuration to disable Wrs::Events
  # config.disable = true                                         # optional

  # List of topics allowed for publishing. You can leave the `topics` key empty if you don't want to publish anything
  config.publish_to = [{
    app_name: 'app_which_generates_these_topics', topics: %w[
      topic_name_1
      topic_name_2
    ]
  }]
  
  # List of topics to be subscribed to. You can leave the `topics` key empty if you don't want to receive anything
  config.subscribe_to = [{
    app_name: 'app_which_generates_these_topics', topics: %w[
      topic_name_1
      topic_name_2
    ]
  }]
end

Wrs::Events::Service.initiate

```

Then, add `events` queue to `config/sidekiq.yml` in this order:
```ruby
:queues:
  - critical
  - events # Add dedicated 'events' queue to listen to messages
  - default
```

## Usage

After the setup, you can publish events listed in `config.publish_to` by:
```ruby
Wrs::Events::Service.publish(topic_name, primary_object, message_hash)
```
#### Parameter description
  * `topic_name`: Name of the topic to publish your event (eg. topic_name_1)
  * `primary_object`: Primary object associated with the event.  
    * For eg. if the event_type is 'user_update', primary_object will be the user object getting updated 
  * `message_hash`: Event body containing the details

If you've subscribed to a topic which received a event(using `config.subscribe_to`), your `EventHandler.process_messages` method will be triggered. This should have the code to process your messages

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/metacubelabs/wrs-events. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Wrs::Events project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/wrs-events/blob/master/CODE_OF_CONDUCT.md).
