# frozen_string_literal: true

module Wrs
  module Events

    class Configuration < Base
      attr_accessor :aws_access_key_id, :aws_secret_access_key, :aws_region, :app_name, :queue_suffix, :event_handler, :publish_to, :subscribe_to, :disable

      def initialize
        # puts "config_initialize: "
        @aws_access_key_id = nil
        @aws_secret_access_key = nil
        @aws_region = nil
        @app_name = nil
        @queue_suffix = Rails.env
        @event_handler = 'EventHandler'
        @disable = false
        @publish_to = []
        @subscribe_to = []
      end

      def initialize_platform
        # puts "initialize_platform: "
        configure_platform
        initialize_platform_clients
      end

      def configure_platform
        # puts "configure_platform: "
        Aws.config.update(
          credentials: Aws::Credentials.new(@aws_access_key_id, @aws_secret_access_key),
          region: @aws_region
        )
      end

      def initialize_platform_clients
        # puts "initialize_platform_clients: "
        { publisher: Aws::SNS::Client.new, subscriber: Aws::SQS::Client.new }
      end

      def valid?
        super
        aws_credentials_valid?
      end

      private

      def aws_credentials_valid?
        raise Wrs::Events::MissingAWSCredentialError if @aws_access_key_id.blank? || @aws_secret_access_key.blank? || @aws_region.blank?
      end

    end

  end
end
