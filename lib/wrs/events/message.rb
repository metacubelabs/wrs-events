# frozen_string_literal: true

module Wrs
  module Events
    class Message < Base
      attr_accessor :subject, :action, :data, :meta, :message_attributes

      class << self
        def from_platform_payload raw_payload
          msg_h = JSON.parse(JSON.parse(raw_payload)['Message']).with_indifferent_access
          message_attributes = JSON.parse(raw_payload)['MessageAttributes']&.with_indifferent_access|| {}
          new(msg_h['subject'], msg_h['action'], msg_h['data'], msg_h['meta'], message_attributes )
				rescue StandardError
          Rails.logger.error("Error: Payload => #{JSON.parse(raw_payload)['Message']}")
          new({}, {}, {}, {}, {})
        end
      end

      def initialize subject, action, data = {}, meta = {}, message_attributes = {}
        # puts 'message_initialize: '
        @subject = subject
        @action = action
        @data = data
        @meta = meta
        @message_attributes = message_attributes
        @errors = []
      end

      def valid?
        super
        subject_valid? && action_valid? && data_valid?
      end

      def to_json *_args
        { subject: subject, action: action, data: data, meta: meta, message_attributes: message_attributes }.to_json
      end

      private

      def subject_valid?
        (subject.present? && subject.is_a?(Hash) && subject[:id].present? && subject[:type].present?) ? true : add_errors(:subject)
      end

      def action_valid?
        (action.present? && action.is_a?(Hash) && action[:type].present? && action[:app_name].present?) ? true : add_errors(:action)
      end

      def data_valid?
        data.present? && data.is_a?(Hash) ? true : add_errors(:data)
      end

    end
  end
end
