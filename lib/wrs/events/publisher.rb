# frozen_string_literal: true

module Wrs
  module Events
    class Publisher

      class << self

        attr_accessor :client, :topics, :errors

        def initiate
          # puts "publisher_initiate: "
          self.client ||= Wrs::Events.publisher
          return if Wrs::Events.configuration.disable

          initialize_topics_for_publication
        end

        def publish message, event_type, msg_attribute = {}
          return if Wrs::Events.configuration.disable

          return message.errors if message.invalid?

          return "Permission denied to publish to topic '#{event_type}'" unless can_publish_to?(event_type)

          publishing_topic = absolute_topic_name event_type

          publish_to_arn = topic_uid(publishing_topic)
          return "Can't fetch topic with name '#{event_type}'" if publish_to_arn.blank?

          publish_options = {
            topic_arn: publish_to_arn,
            message: message.to_json
            # message_attributes: {
            #   "String" => {
            #     data_type: "String",
            #     string_value: "TestMsg"
            #   },
            # },
          }
          # Add message attributes if provided
          unless msg_attribute.empty?
            publish_options[:message_attributes] = convert_attributes(msg_attribute)
          end
        
          # Publish to the topic
          self.client.publish(publish_options)
        end

        def find_or_create_topic topic_name, app_name=nil
          formatted_topic_name = app_name ? absolute_topic_name(topic_name, app_name) : topic_name
          self.client.create_topic(
            name: formatted_topic_name
          )
        end

        def delete_topic topic_name, app_name=nil
          formatted_topic_name = app_name ? absolute_topic_name(topic_name, app_name) : topic_name
          topic_arn = topics.fetch(formatted_topic_name, {}).fetch(:url, nil)
          return if topic_arn.blank?
          
          self.client.delete_topic(
            topic_arn: topic_arn
          )
        end

        def get_all_topics
          res = client.list_topics
          all_topics = res.topics
          puts "\nres#{res.topics.present?}---- #{res.next_page?} ---- #{all_topics.count}"
          while res.next_page? do
            res = res.next_page
            all_topics = all_topics.concat(res.topics)
            puts "\nres#{res.topics.present?}---- #{res.next_page?} ---- #{all_topics.count}"
          end
          all_topics.map(&:topic_arn)
        end

        def publication_list
          Wrs::Events.configuration.publish_to.map do |s|
            s[:topics].map { |t| absolute_topic_name(t, s[:app_name]) }
          end.flatten
        end

        def topic_uid name
          Wrs::Events::Subscriber.topics[name][:url] if Wrs::Events::Subscriber.topics[name].present?
          topics[name][:url] if topics[name].present?
        end

        def absolute_topic_name topic, app_name=Wrs::Events.configuration.app_name
          return nil if topic.blank?

          [app_name, Wrs::Events.configuration.queue_suffix, topic].join('_')
        end

        private

        def can_publish_to? event_type
          publication_list.include? absolute_topic_name(event_type)
        end

        def initialize_topics_for_publication
          self.topics ||= {}
          topics_to_publish = Wrs::Events::Publisher.publication_list
          # puts topics_to_publish.inspect
          topics_to_publish.each do |topic_name|
            res = find_or_create_topic(topic_name)
            raise StandardError if res.blank?

            self.topics[topic_name] = {
              url: res.topic_arn
            }
          end
        end

        def convert_attributes(msg_attribute)
          msg_attribute.compact.each_with_object({}) do |(key, value), result|

            data_type = infer_data_type(value)
    
            result[key.to_s] = {
              data_type: data_type  # Use "DataType" as per the SNS format
            }
            
            if data_type == "String.Array"
              result[key.to_s][:string_value] = JSON.generate(value)
            else
              result[key.to_s][:string_value] = value.to_s
            end
          end
        end
        
        def infer_data_type(value)
          case value
          when String, Date, DateTime, Time, TrueClass, FalseClass then "String"
          when Integer, Float then "Number"
          when Array then "String.Array"
          else
            raise ArgumentError, "Data type not supported for value: #{value.inspect} (#{value.class})"
          end
        end
      end

    end
  end
end