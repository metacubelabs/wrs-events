# frozen_string_literal: true

module Wrs
  module Events

    class Errors
    end
    class Error < StandardError
      def initialize(message)
        super(message)
      end
    end

    class MissingAWSCredentialError < Wrs::Events::Error
      def initialize
        @message = 'Missing AWS credentials. Please provide valid access keys and region'
        super(@message)
      end
    end

    class MissingPlatformClient < Wrs::Events::Error
      def initialize
        @message = 'Platform publish/subscribe clients are missing'
        super(@message)
      end
    end
  end
end
