# frozen_string_literal: true

module Wrs
  module Events
    class Service
      class << self
        def initiate
          Wrs::Events::Publisher.initiate
          Wrs::Events::Subscriber.initiate

          start_listening
          Rails.logger.info 'WRS::Events platform setup completed'
        end

        def start_listening
          Sidekiq.configure_server do |config|
            config.on(:startup) do
              # Rails.logger.info "Startup hook initiated..."
              Wrs::Events::Subscriber.start_listener
              # Rails.logger.info "Startup hook finished..."
            end

            config.on(:shutdown) do
              # Rails.logger.info "Shutdown hook initiated..."
              Wrs::Events::Subscriber.clear_listeners
              # Rails.logger.info "Shutdown hook finished..."
            end
          end
        end

        def create_topic_subscription event_type, app_name
          return "One of topic or app name is missing" if event_type.blank? || app_name.blank?

          res = Wrs::Events::Publisher.find_or_create_topic event_type, app_name
          return "Error while creating topic" if res.blank?

          subs = Wrs::Events::Subscriber.subscribe_to_topic res.topic_arn, event_type
        end

        def delete_topic_subscription event_type, app_name
          return "One of topic or app name is missing" if event_type.blank? || app_name.blank?

          res = Wrs::Events::Publisher.delete_topic event_type, app_name
        end

        def update_topic_subscription event_type, app_name
          delete_topic_subscription event_type, app_name
          create_topic_subscription event_type, app_name
        end

        def publish event_type, subject, data = {}, msg_attribute = {}
          subject_h = { id: subject.try(:id), type: subject.try(:class).try(:name) }
          action_h = { type: event_type, app_name: Wrs::Events.configuration.app_name }
          meta_h = { timestamp: Time.zone.now }
          message_obj = Wrs::Events::Message.new(subject_h, action_h, data, meta_h)
          Wrs::Events::Publisher.publish message_obj, event_type, msg_attribute
        end
      end
    end
  end
end
