# frozen_string_literal: true

module Wrs
  module Events
    class Listener
      include Sidekiq::Worker
      sidekiq_options queue: 'events'

      def perform
        return if Wrs::Events.configuration.disable || Wrs::Events::Subscriber.subscription_list.blank?

        sqs_client = Wrs::Events.subscriber
        res = sqs_client.get_queue_url(
          queue_name: Wrs::Events::Subscriber.application_queue_name
        )
        # puts res.queue_url
        poller = Aws::SQS::QueuePoller.new(res.queue_url)
        Rails.logger.info 'WRS::Events -> Event polling started...'
        # puts poller.inspect
        poller.poll(skip_delete: true) do |raw_payload|
          msg_obj = Wrs::Events::Message.from_platform_payload raw_payload.body
          if msg_obj.message_attributes.present?
            msg_obj.message_attributes = msg_obj.message_attributes.transform_values do |details|
              cast_value(details["Type"], details["Value"])
            end
          end
          Rails.logger.info "Wrs::Events -> poller_triggered: #{msg_obj.action}"
          Wrs::Events::Subscriber.event_handler_class
            .send(Wrs::Events::Subscriber.event_handler_method, msg_obj)
          poller.delete_message(raw_payload)
        end
      rescue StandardError => e
        Rails.logger.error "Error encountered while polling:\n#{e.message.inspect}"
        raise
      end

      def cast_value(type, value)
        case type
        when "String"
          value.to_s
        when "Number"
          value.to_i
        when "Boolean"
          value == "true"
        when "DateTime"
          DateTime.parse(value) rescue value
        else
          value
        end
      end
    end
  end
end
