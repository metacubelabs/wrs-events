# frozen_string_literal: true

module Wrs
  module Events

    class Base
      attr_accessor :errors

      def valid?
        self.errors = []
        true
      end

      def invalid?
        !valid?
      end

      private

      def add_errors key
        errors << key.to_s + ' is invalid'
        false
      end

    end

  end
end
