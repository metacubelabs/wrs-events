# frozen_string_literal: true

module Wrs
  module Events
    class Subscriber

      class << self

        attr_accessor :client, :topics, :queue, :errors

        def initiate
          # puts "subscriber_initiate: "
          self.client ||= Wrs::Events.subscriber
          return if Wrs::Events.configuration.disable

          create_application_queue
          initialize_topics_for_subscription
        end

        def start_listener
          Rails.logger.info "Starting listener..."
          25.times do |i|
            # Rails.logger.info "#{25 - i}..."
            sleep(1)
          end
          return if Wrs::Events.listener_job_id.present?

          clear_listeners

          Wrs::Events.listener_job_id = Wrs::Events::Listener.perform_at(Time.zone.now)
        end

        def clear_listeners
          # Using Sidekiq redis
          queued_jobs = Sidekiq.redis { |r| r.lrange "queue:events", 0, -1 }.select { |j| JSON.parse(j)["class"] == "Wrs::Events::Listener" }
          Rails.logger.info "#{queued_jobs.count} Jobs...#{queued_jobs.join('\n').inspect}"
          queued_jobs.each { |job| Sidekiq.redis { |r| r.lrem "queue:events", -1, job } }
          
          # Sidekiq::Queue.new('events').clear
        end

        def subscribe_to_topic topic_arn, topic_name
          new_topic = topics[topic_name].blank? || topics[topic_name][:url].blank?
          self.topics[topic_name] = { url: topic_arn } if new_topic

          Wrs::Events::Publisher.client.subscribe(
            topic_arn: topic_arn,
            protocol: SUBSCRIBER_PROTOCOL,
            endpoint: application_queue_arn,
            return_subscription_arn: false,
          )

          # DEBUG: Topic-wise permissions are disabled as with dynamic event subscription support, a list of subsciptions needs to be maintained in order to support it.
          # permit_topics(topics.values.map { |topic| topic[:url] }) if new_topic
        end

        def permit_topics topic_arns=[]
          self.client.set_queue_attributes(
            queue_url: application_queue_url,
            attributes: {
              "Policy": policy_template_allow_topics(application_queue_arn, topic_arns)
            }
          )
        end

        def subscription_list
          Wrs::Events.configuration.subscribe_to.map do |s|
            s[:topics].map do |t|
              matcher = matching_pattern(t)
              if matcher.present?
                all_arns = Wrs::Events::Publisher.get_all_topics.select do |t_arn|
                  t_arn.include?(Wrs::Events::Publisher.absolute_topic_name(matcher[1], s[:app_name]))
                end
                all_arns.map { |t_arn| topic_name_from_arn(t_arn) }
              else
                Wrs::Events::Publisher.absolute_topic_name(t, s[:app_name])
              end
            end
          end.flatten
        end

        def matching_pattern topic_name
          topic_name.match(/(.*?)(\-\*\-?)(.*)/)
        end

        def topic_name_from_arn t_arn
          t_arn&.split(':')&.last
        end

        def application_queue_name
          # puts "get_namespaced_queue_name: "
          [Wrs::Events.configuration.app_name, Wrs::Events.configuration.queue_suffix].join('_')
        end

        def application_queue_arn
          queue[:attributes].attributes['QueueArn']
        end

        def application_queue_url
          queue[:properties].url
        end

        # TODO: Create WRS Queue Wrapper
        def get_queue_details queue_name
          aws_resource = Aws::SQS::Resource.new(client: self.client)
          queue = aws_resource.get_queue_by_name(queue_name: queue_name)
          # puts queue
          attributes = self.client.get_queue_attributes(queue_url: queue.url, attribute_names: ['All'])
          # puts attributes
          { properties: queue, attributes: attributes }
        end

        def event_handler_class
          Wrs::Events.configuration.event_handler.safe_constantize
        end

        def event_handler_method
          'process_messages'
        end

        # TODO: Create Wrapper classes to hold templates
        def policy_template_allow_topics queue_arn, topic_arns=[]
          # DEBUG: Topic-wise permissions are disabled as with dynamic event subscription support, a list of subsciptions needs to be maintained in order to support it.
          {
            "Version": '2012-10-17',
            "Statement": [
              {
                "Effect": 'Allow',
                "Principal": '*',
                "Action": 'SQS:SendMessage',
                "Resource": queue_arn,
                "Condition": {
                  "ArnLike": {
                    "aws:SourceArn": "arn:aws:sns:" + Wrs::Events.configuration.aws_region + ":*:*"
                  }
                }
              }
            ]
          }.to_json
        end

        private

        def create_application_queue
          res = self.client.create_queue(
            queue_name: application_queue_name
          )
          raise "Application queue can't be initialized" unless res.present? && res.queue_url.present?
          
          set_application_queue
          permit_topics
        end

        def set_application_queue
          self.queue = get_queue_details application_queue_name
        end

        def initialize_topics_for_subscription
          self.topics ||= {}
          topics_to_subscribe = subscription_list
          # puts topics_to_subscribe.inspect
          topics_to_subscribe.each do |topic_name|
            res = Wrs::Events::Publisher.client.create_topic(
              name: topic_name
            )
            raise StandardError if res.blank?

            self.topics[topic_name] = {
              url: res.topic_arn
            }
          end

          create_queue_subscriptions
        end

        def create_queue_subscriptions
          # puts "create_subscriptions: #{SUBSCRIBER_PROTOCOL}"
          topics.each do |name, props|
            subscription = subscribe_to_topic props[:url], name
            # puts "Subscribed to: #{subscription.inspect}"
          end
          # DEBUG: Topic-wise permissions are disabled as with dynamic event subscription support, a list of subsciptions needs to be maintained in order to support it.
          # permit_topics(self.topics.values.map { |topic| topic[:url] })
        end

      end

    end
  end
end
