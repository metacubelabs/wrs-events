# frozen_string_literal: true

Gem.loaded_specs['wrs-events'].dependencies.each do |d|
  require d.name if d.type == :runtime
end

require 'wrs/events/version'
require 'wrs/events/base'
require 'wrs/events/configuration'
require 'wrs/events/error'
require 'wrs/events/message'
require 'wrs/events/service'
require 'wrs/events/subscriber'
require 'wrs/events/publisher'
require 'wrs/events/listener'

module Wrs
  module Events
    SUBSCRIBER_PROTOCOL = 'sqs'
    PUBLISHER_PROTOCOL = 'sns'

    class << self
      attr_accessor :configuration, :publisher, :subscriber, :application_queue_url, :listener_job_id, :errors

      def configure
        self.configuration ||= Configuration.new
        yield(configuration)
        clients ||= configuration.initialize_platform

        self.publisher = clients[:publisher] if clients.present?
        self.subscriber = clients[:subscriber] if clients.present?
      end

      def reset
        self.configuration = Configuration.new
        self.publisher = nil
        self.subscriber = nil
      end
    end
  end
end
